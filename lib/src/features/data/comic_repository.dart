import 'dart:convert';
import 'package:comic_book/src/features/models/comic.dart';
import 'package:comic_book/src/features/models/comic_detail.dart';
import 'package:http/http.dart' as http;

class ComicRepository {
  final String urlGlobal =
      'https://comicvine.gamespot.com/api'; // I declare the variable for a better handling
  final String apiForMe =
      '47b1165a136d69afd5a65188fe8a46dfc5b97bb0'; // This is my key

  Future getAllComics() async {
    // With this method I call all the comics

    try {
      var response = await http.get(
        Uri.parse('$urlGlobal/issues/?api_key=$apiForMe&format=json'),
        headers: {
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT, DELETE, HEAD",
        },
      );

      if (response.statusCode == 200) {
        Map responseData = json.decode(response.body);
        List comics = responseData['results']
            .map(
              (comicsJson) => Comic.fromMap(
                  comicsJson), // Here I make a mapping of the data received to convert in a list of type 'Comic'
            )
            .toList();
        return comics; // Here I return the data list of all comics
      } else {
        return null; // If something failed, here it returns null to validate it in cubit
      }
    } catch (e) {
      return null;
    }
  }

  Future getSpecificComic(String apiDetail) async {
    // With this method I call an specific comics and I send the issue by parameter

    try {
      var response = await http.get(Uri.parse(
              '$apiDetail?api_key=$apiForMe&format=json') // Here I concatenate the string to make the query
          );

      if (response.statusCode == 200) {
        Map responseData = json.decode(response.body);
        Map comicDetail = responseData['results'];
        return ComicDetail.fromMap(
            comicDetail); // Here I return the data of that comic
      } else {
        return null; // If something failed, here it returns null to validate it in cubit
      }
    } catch (e) {
      return null;
    }
  }
}
