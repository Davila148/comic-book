import 'package:flutter/material.dart';
// This class will help us with the states of the app
@immutable
class ComicState {
  final String error;
  final bool success;
  final bool isConnected;

  const ComicState({
    this.success = false,
    this.error = '',
    this.isConnected = true,
  });
}
