import 'package:comic_book/src/features/models/comic_detail.dart';
import 'package:comic_book/src/features/ui/screens/comic/comic_cubit.dart';
import 'package:comic_book/src/features/ui/screens/comic/comic_state.dart';
import 'package:comic_book/src/features/ui/widgets/app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';

class ComicDetailPage extends StatefulWidget {
  // ignore: prefer_typing_uninitialized_variables
  final comicSpecified;
  const ComicDetailPage({Key? key, required this.comicSpecified})
      : super(key: key);

  @override
  State<ComicDetailPage> createState() => _ComicDetailPageState();
}

class _ComicDetailPageState extends State<ComicDetailPage> {
  bool loading = false; // Variable to change if the screen is loading or not
  bool isVisible =
      false; // Variable to change the visibility when the data is load successful
  ComicDetail comicData =
      ComicDetail(); // In this instance will be obtain the comics data
  bool isList = false; // To validate if is list or is grid

  uploadData(ComicCubit cubit) async {
    // Here I get the api detail and then send it as a parameter for the query
    final apiDetail = widget.comicSpecified.apiDetail;
    comicData = await cubit.getDataSpecifiedComic(apiDetail);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .size; // It will help me for make a responsive design
    return LoadingOverlay(
      isLoading: loading,
      child: BlocProvider(
        create: (_) => ComicCubit()..isConnectedToInternet(),
        child: BlocConsumer<ComicCubit, ComicState>(
          listener: (context, state) {
            // State Management
            if (!state.success && state.error != '') {
              setState(() {
                loading = false;
              });
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  content: Text(state.error),
                ),
              );
            }
            if (state.success) {
              setState(() {
                loading = false;
                isVisible = true;
              });
            }
            if (!state.isConnected) {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  content: Text(state.error),
                ),
              );
            }
            if (!state.success && state.error == '' && state.isConnected) {
              ComicCubit cubit = BlocProvider.of<ComicCubit>(context);
              setState(() {
                loading = true;
              });
              uploadData(cubit);
            }
          },
          builder: (context, snapshot) {
            return Scaffold(
              backgroundColor: Colors.grey[200],
              appBar: appBarGeneral(),
              body: Visibility(
                // If the data is obtained successfully, it will allow you to see the widgets
                visible: isVisible,
                child: ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: size.height * 0.01,
                        left: size.width * 0.02,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: size.width * 0.38,
                            child: Column(
                              children: [
                                title('Characters', size),
                                wrapWithGrid(size, comicData.characterCredits),
                                SizedBox(height: size.height * 0.1),
                                title('Teams', size),
                                wrapWithGrid(size, comicData.teamCredits),
                                SizedBox(height: size.height * 0.1),
                                title('Locations', size),
                                wrapWithGrid(size, comicData.locationCredits),
                                SizedBox(height: size.height * 0.1),
                                title('Concepts', size),
                                wrapWithGrid(size, comicData.conceptCredits),
                                SizedBox(height: size.height * 0.1),
                              ],
                            ),
                          ),
                          image(size),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget wrapWithGrid(Size size, List list) {
    // Here we find the widget that show us the characters, teams, locations and concepts
    return Align(
      alignment: Alignment.centerLeft,
      child: Wrap(
        alignment: WrapAlignment.spaceBetween,
        children: grid(size, list),
      ),
    );
  }

  Widget image(Size size) {
    // This widget show us the image in the screen
    return Container(
      width: size.width * 0.6,
      height: size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(
            comicData.image,
          ),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget title(String text, Size size) {
    // This widget is for show us the title for each section (characters, teams, locations and concepts)
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          text,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w900,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 5),
          height: 1,
          width: size.width * 0.35,
          color: Colors.grey,
        ),
      ],
    );
  }

  List<Widget> grid(Size size, List list) {
    // This widget will return us the information for each section
    List<Widget> listGrid = [];
    for (var i = 0; i < list.length; i++) {
      listGrid.add(
        Container(
          width: size.width * 0.17,
          margin: const EdgeInsets.only(top: 5, left: 15),
          child: Text(
            list[i]['name'],
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: size.width > 900 ? 18 : 14,
              color: Colors.green[200],
            ),
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
          ),
        ),
      );
    }
    return listGrid;
  }
}
