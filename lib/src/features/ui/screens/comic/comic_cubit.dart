import 'package:comic_book/src/features/data/comic_repository.dart';
import 'package:comic_book/src/features/ui/screens/comic/comic_state.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ComicCubit extends Cubit<ComicState> {
  ComicCubit() : super(const ComicState());
  final comicRepository = ComicRepository();

  void isConnectedToInternet() async {
    // This method is to verify if the device is connected or not to internet and emit a status error
    ConnectivityResult isConnected = await Connectivity().checkConnectivity();
    if (isConnected == ConnectivityResult.none) {
      emit(
        const ComicState(
          error: 'The device is without connection to internet.',
          isConnected: false,
          success: false,
        ),
      );
    } else {
      // Here I emit the state by default, therefore it will call the data
      emit(const ComicState());
    }
  }

  Future getAllComicsData() async {
    // This method return a state which will receive the page
    final comicsData = await comicRepository.getAllComics();
    if (comicsData != null) {
      emit(const ComicState(success: true));
      return comicsData;
    } else {
      emit(const ComicState(error: 'Failed to get the data.'));
    }
  }

  Future getDataSpecifiedComic(String apiDetail) async {
    // This method return a state which will receive the page
    final dataSpecifiedComic = await comicRepository.getSpecificComic(apiDetail);
    if (dataSpecifiedComic != null) {
      emit(const ComicState(success: true));
      return dataSpecifiedComic;
    } else {
      emit(const ComicState(error: 'Failed to get the data.'));
    }
  }
}
