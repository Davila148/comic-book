import 'package:comic_book/src/features/ui/screens/comic/comic_cubit.dart';
import 'package:comic_book/src/features/ui/screens/comic/comic_state.dart';
import 'package:comic_book/src/features/ui/widgets/app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:comic_book/router/router_path.dart' as routes;

class ComicPage extends StatefulWidget {
  const ComicPage({Key? key}) : super(key: key);

  @override
  State<ComicPage> createState() => _ComicPageState();
}

class _ComicPageState extends State<ComicPage> {
  bool loading = false; // Variable to change if the screen is loading or not
  bool isVisible =
      false; // Variable to change the visibility when the data is load successful
  List comicData = []; // In this list will be obtain the comics data
  bool isList = false; // To validate if is list or is grid

  uploadData(ComicCubit cubit) async {
    // Here I call the data
    comicData = await cubit.getAllComicsData();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .size; // It will help me for make a responsive design
    return LoadingOverlay(
      isLoading: loading,
      child: BlocProvider(
        create: (_) => ComicCubit()..isConnectedToInternet(),
        child: BlocConsumer<ComicCubit, ComicState>(
          listener: (context, state) {
            // State Management
            if (!state.success && state.error != '') {
              // State when there is an error
              setState(() {
                loading = false;
              });
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  content: Text(state.error),
                ),
              );
            }
            if (state.success) {
              // Status when the data has been called successful
              setState(() {
                loading = false;
                isVisible = true;
              });
            }
            if (!state.isConnected) {
              // Status when the app is without connection to internet
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  content: Text(state.error),
                ),
              );
            }
            if (!state.success && state.error == '' && state.isConnected) {
              // This is the status by default and then I call the method to
              // get the data
              ComicCubit cubit = BlocProvider.of<ComicCubit>(context);
              setState(() {
                loading = true;
              });
              uploadData(cubit);
            }
          },
          builder: (context, snapshot) {
            ComicCubit cubit = BlocProvider.of<ComicCubit>(context);
            return Scaffold(
              backgroundColor: Colors.grey[200],
              appBar: appBarGeneral(),
              body: Visibility(
                // If the data is obtained successfully, it will allow you to see the widgets
                visible: isVisible,
                child: Stack(
                  children: [
                    // The refresh indicator will help us to refresh the data in any moment calling back the method
                    RefreshIndicator(
                      onRefresh: () => uploadData(
                          cubit), // Here I calling back the method to get the data
                      child: Container(
                        margin: EdgeInsets.only(top: size.height * 0.05),
                        child: ListView(
                          children: [
                            !isList
                                ? Wrap(
                                    alignment: WrapAlignment.center,
                                    children: comic(size),
                                  )
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: comicInList(size),
                                  )
                          ],
                        ),
                      ),
                    ),
                    // This is the filter where I can select if I see the data in list or in a grid
                    filter(size),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  onTapPush(var comic) {
    // This is to navigate to the other screen (Comic Detail)
    Navigator.pushNamed(
      context,
      routes.detailComicPageRoute,
      arguments: {
        'comic': comic,
      },
    );
  }

  Widget typeView(
    String text,
    IconData icon,
    dynamic onTap,
    Size size,
    bool isGreen,
  ) {
    // This is the text to change if I can see the data in a list or in a grid
    return InkWell(
      onTap: onTap, // Here I call the method to switch view
      child: Row(
        children: [
          Icon(
            icon,
            size: size.height * 0.02,
            color: isGreen
                ? Colors.green[200]
                : Colors
                    .black, // If is selected is black color, otherwise will be green
          ),
          SizedBox(width: size.width * 0.001), // A small space
          Text(
            text,
            style: TextStyle(
              color: isGreen
                  ? Colors.green[200]
                  : Colors
                      .black, // If is selected is black color, otherwise will be green
            ),
          ),
        ],
      ),
    );
  }

  Widget filter(Size size) {
    // This is the widget where are the filter and the subtitle 'Latest Issues'
    return Container(
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        border: Border.all(
          color: Colors.grey,
          width: 1,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'Latest Issues',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          Row(
            children: [
              typeView(
                'List',
                Icons.list,
                // Here I sent the function by parameter
                () {
                  setState(() {
                    isList = true;
                  });
                },
                size,
                !isList
                    ? true
                    : false, // To validate if will be green or black color
              ),
              SizedBox(width: size.width * 0.015),
              typeView(
                'Grid',
                Icons.grid_on_rounded,
                // Here I sent the function by parameter
                () {
                  setState(() {
                    isList = false;
                  });
                },
                size,
                !isList
                    ? false
                    : true, // To validate if will be green or black color
              )
            ],
          ),
        ],
      ),
    );
  }

  List<Widget> comicInList(Size size) {
    // This widget will return us the information if the list type view is selected
    List<Widget> listComics = [];
    for (var i = 0; i < comicData.length; i++) {
      // Here I fill a list widgets to then return it
      listComics.add(
        Column(
          children: [
            InkWell(
              onTap: () =>
                  onTapPush(comicData[i]), // To navigate to comic detail
              child: Container(
                width: size.width,
                height: size.height * 0.3,
                margin: const EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.network(comicData[i].image),
                    Flexible(
                      child: SizedBox(
                        width: size.width * 0.4,
                        child: Column(
                          children: [
                            Text(
                              comicData[i].name +
                                  ' #' +
                                  comicData[i].issueNumber,
                              style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            dateFormat(comicData[i].date),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            const Divider(),
          ],
        ),
      );
    }
    return listComics; // Here I return the list widgets to the screen
  }

  List<Widget> comic(Size size) {
    // This widget will return us the information if the grid type view is selected
    List<Widget> listComics = [];
    for (var i = 0; i < comicData.length; i++) {
      // Here I fill a list widgets to then return it
      listComics.add(
        InkWell(
          onTap: () => onTapPush(comicData[i]), // To navigate to comic detail
          child: Container(
            width: size.width * 0.22,
            margin: const EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.network(
                  comicData[i].image,
                  height: size.width > 900
                      ? size.height * 0.25
                      : size.height * 0.15,
                ),
                SizedBox(height: size.height * 0.01),
                Text(
                  comicData[i].name + ' #' + comicData[i].issueNumber,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
                dateFormat(comicData[i].date),
              ],
            ),
          ),
        ),
      );
    }
    return listComics; // Here I return the list widgets to the screen
  }

  Widget dateFormat(String date) {
    // This widget is to validate the date and display it as the design
    String day = date.substring(8, 10); // To get only the day
    String month = date.substring(5, 7); // To get only the month
    String year = date.substring(0, 4); // To get only the year
    switch (month) {
      case '01':
        month = 'January';
        break;
      case '02':
        month = 'February';
        break;
      case '03':
        month = 'March';
        break;
      case '04':
        month = 'April';
        break;
      case '05':
        month = 'May';
        break;
      case '06':
        month = 'June';
        break;
      case '07':
        month = 'July';
        break;
      case '08':
        month = 'August';
        break;
      case '09':
        month = 'September';
        break;
      case '10':
        month = 'October';
        break;
      case '11':
        month = 'November';
        break;
      case '12':
        month = 'December';
        break;
      default:
        month = 'Month';
    }
    return Text(
      month + ' ' + day + ', ' + year,
      textAlign: TextAlign.center,
    );
  }
}
