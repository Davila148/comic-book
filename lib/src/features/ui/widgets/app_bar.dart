import 'package:flutter/material.dart';

PreferredSizeWidget appBarGeneral() {
  // How the appbar is the same for both screens I decided to make it a widget
  return AppBar(
    iconTheme: const IconThemeData(color: Colors.black),
    backgroundColor: Colors.grey[200],
    elevation: 0,
    centerTitle: true,
    title: const Text(
      'ComicBook',
      style: TextStyle(
        color: Colors.black,
        fontSize: 22,
        fontWeight: FontWeight.w400,
      ),
    ),
  );
}
