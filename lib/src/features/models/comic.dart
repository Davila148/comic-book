class Comic {
  String image;
  String date;
  String name;
  String issueNumber;
  String apiDetail;

  Comic({
    // In Flutter 2.0, any variable can be null, that's why I initialize with empty strings
    this.image = '',
    this.date = '',
    this.name = '',
    this.issueNumber = '',
    this.apiDetail = '',
  });

  factory Comic.fromMap(Map map) {
    return Comic(
      image: map['image']['original_url'],
      date: map['date_added'],
      // If the name is null, This will return an empty string
      name: map['name'] ?? '',
      issueNumber: map['issue_number'],
      apiDetail: map['api_detail_url'],
    );
  }

  Map<String, dynamic> toMap() {
    // This will help me if I want to send data to the API
    return <String, dynamic>{
      'image': image,
      'date': date,
      'name': name,
      'issueNumber': issueNumber,
      'apiDetail': apiDetail,
    };
  }
}
