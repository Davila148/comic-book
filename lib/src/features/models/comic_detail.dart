class ComicDetail {
  String image;
  List characterCredits;
  List teamCredits;
  List locationCredits;
  List conceptCredits;

  ComicDetail({
    // In Flutter 2.0, any variable can be null, that's why I initialize with empty strings
    this.image = '',
    this.characterCredits = const [],
    this.teamCredits = const [],
    this.locationCredits = const [],
    this.conceptCredits = const [],
  });

  factory ComicDetail.fromMap(Map map) {
    return ComicDetail(
      image: map['image']['original_url'],
      // If the lists are null, This will return an empty list
      characterCredits: map['character_credits'] ?? [],
      teamCredits: map['team_credits'] ?? [],
      locationCredits: map['location_credits'] ?? [],
      conceptCredits: map['concept_credits'] ?? [],
    );
  }

  Map<String, dynamic> toMap() {
    // This will help me if I want to send data to the API
    return <String, dynamic>{
      'image': image,
      'characterCredits': characterCredits,
      'teamCredits': teamCredits,
      'locationCredits': locationCredits,
      'conceptCredits': conceptCredits,
    };
  }
}
