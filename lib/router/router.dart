import 'package:comic_book/main.dart';
import 'package:comic_book/router/router_path.dart' as routes;
import 'package:comic_book/src/features/ui/screens/comic/comic_detail_page.dart';
import 'package:comic_book/src/features/ui/screens/comic/comic_page.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  // This is the router of the app, here you will control the navigation of the application
  switch (settings.name) {
    case routes.comicPageRoute:
      return MaterialPageRoute(
        builder: (context) => const ComicPage(),
      );
    case routes.detailComicPageRoute:
      // Here I get the parameter sent
      final comic = (settings.arguments as Map)['comic'];
      return MaterialPageRoute(
        builder: (context) => ComicDetailPage(comicSpecified: comic),
      );
    default:
      return MaterialPageRoute(
        builder: (context) => const MyApp(),
      );
  }
}
