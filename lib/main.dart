import 'package:comic_book/src/features/ui/screens/comic/comic_page.dart';
import 'package:flutter/material.dart';
import 'package:comic_book/router/router_path.dart' as routes;
import 'package:comic_book/router/router.dart' as router;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Comic Book Application',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: routes.comicPageRoute,
      onGenerateRoute: router.generateRoute,
      home: const ComicPage(),
    );
  }
}
